import javax.swing.*;

public class Notepad extends JFrame {

    public Notepad() {

        JMenuBar menuBar = Menubar.getMenuBar();

        add(menuBar);

        setTitle("Welcome To Notepad");
        setSize(600, 600);
        setLayout(null);
        setVisible(true);

    }

    public static void main(String[] args) {
        new Notepad();
    }

}
